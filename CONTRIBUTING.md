Welcome to the MiniWS team,
We are always looking for ideas you this project! if you would like to point out any flaws in our design, or simply add to it, feel free to raise your concerns on Gitlab.
For all contributions, please send an email to contributions@miniws.com with your idea and supporting documentation detailing your changes/additions.
A member of our team will get back to you within one business week.
But, in the case of wanting to get ahead with changes that you think are pertinent, we are happy for you to get going with development. In this case, please submit a pull request for approval.
Once your contribution is finished, please submit a push request for final review.
Bugs and Fixes
If you encounter issues with this Repo, please log an issue here: issues@miniws.com
Please include the pertinent details in a short description as well as schematics or code you think will be relevant.
We will endeavour to solve them as soon as possible.
Format:

Changes to MiniWS design Schematic and PCB layout must be done in KiCad.
CubeIDE is preferable for all software related queries.
English (UK) is preferable for all documentation.
